﻿Public Class frmSysInput

#Region "Declarations"

    'Public cntInput As Integer
    'Public cntOutput As Integer

    Public scrnKey As String
    Public dt As New DataTable


    'Public dtInput As New DataTable ' table of selected outputs

#End Region

#Region "Events"

    ' reset values of output and timer
    Private Sub btnRefreshTTable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshTTable.Click

        Dim userCtrl As ctrlSysInput = tabSys.SelectedTab.Controls("ctrl" & tabSys.SelectedIndex)

        ' Truth Table
        For Each row As DataRow In userCtrl.dtIn.Rows
            For colIdx As Integer = 1 To userCtrl.cntOutput
                row.Item("O" & colIdx) = "0"
            Next
        Next

        If userCtrl.dtDelay IsNot Nothing AndAlso userCtrl.dtDelay.Rows.Count > 0 Then
            ' Delay table
            For Each row As DataRow In userCtrl.dtDelay.Rows
                For colIdx As Integer = 1 To userCtrl.cntOutput
                    row.Item("DELAY") = "0"
                Next
            Next
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub


    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click

        Dim dcOutput As New Dictionary(Of String, ArrayList)
        Dim dcMaxCount As New Dictionary(Of String, Integer)
        Dim dcTimer As New Dictionary(Of String, ArrayList)
        
        'get input in truth table from ALL systems
        getAllSystemInputs()

        ' combine inputs per output per system
        For sysCtr As Integer = 0 To tabSys.TabPages.Count - 1
            Dim lstAllOutput As New ArrayList ' arraylist of arraylist of outputs
            Dim maxOutCnt As Integer = 0

            ' all input of all output in one system
            Dim lstAllInput As ArrayList = dcLstInput.Item("sys" & sysCtr)
            Dim userCtrl As ctrlSysInput = tabSys.TabPages("sys" & sysCtr).Controls("ctrl" & sysCtr)
            Dim lstPI As New ArrayList

            For outCtr As Integer = 0 To userCtrl.cntOutput - 1
                ' input rows where output value is "1"
                Dim lstInputRows As ArrayList = lstAllInput(outCtr)

                ' COMBINE and get Prime Implicants
                Dim isCombine As Boolean = True
                Dim lstCombinedInputs As New ArrayList ' arraylist of combinedInputs

                ' continue combining until cannot combine anymore
                While (isCombine = True)
                    lstInputRows = combine(lstInputRows, userCtrl.cntInput, lstCombinedInputs, isCombine)

                    'unCombined Inputs can be placed in lstPI
                    lstPI.AddRange(lstInputRows)

                    ' combined inputs will be tried to combined again
                    If lstCombinedInputs.Count > 0 Then
                        lstInputRows = lstCombinedInputs.Clone
                        lstCombinedInputs = New ArrayList
                    End If

                End While

                ' until here is OK..
                ' ------------------------

                ' get EPIs....
                ' map <pi, list of inputs covered>
                Dim dctPI As New Dictionary(Of String, ArrayList)
                For Each pi As String In lstPI
                    ' dctPI = pi, lstInput
                    createDctPI(dctPI, pi, lstAllInput(outCtr))
                Next

                ' TODO plot prime implicant charts and determine PI
                ' map <input, list of PIs>
                Dim dctInputs As New Dictionary(Of String, ArrayList)
                ' list of essential PIs
                Dim lstEPI As New ArrayList

                ' dctInputs & get essential prime implicants
                For Each input As String In lstAllInput(outCtr)
                    ' dctInputs = input, lstPI
                    createDctInput(dctInputs, input, lstPI, lstEPI)
                Next

                ' until here is OK.. 1/20/2015
                ' ------------------------

                '------------------------ LOOP THIS....↓↓↓↓
                ' get EPI(unique PI per input/column) then adjust dctInput and dctPI until no more EPI (unique per input/column)

                ' Based on EPI list, update list of PI & inputs, those not covered by EPI
                Dim flgAddEPI As Boolean = True
                While flgAddEPI = True
                    getEPI(dctInputs, lstEPI, flgAddEPI)
                    updateDcts(dctInputs, dctPI, lstEPI)
                End While

                '------------------------ LOOP THIS... ↑↑↑↑


                ' until here is OK.. 1/20/2015???
                ' ------------------------


                ' getPI with most covered inputs and put into lstEPI and update dctInputs
                Dim lstInputKeys() As String = dctInputs.Keys.ToArray()
                For Each inputKey As String In lstInputKeys

                    If Not dctInputs.Keys.Contains(inputKey) Then
                        ' if input is already covered by other PI
                        Continue For
                    End If

                    ' get list of PI of input
                    Dim lstPiInput As ArrayList = dctInputs.Item(inputKey)

                    Dim maxCnt As Integer = 0   ' PI maxCnt of input
                    Dim maxPI As New ArrayList  ' PI with maxCnt

                    ' for each pi, check covered inputs
                    For Each piKey As String In lstPiInput
                        ' list of inputs from dctPI
                        Dim lstInPi As ArrayList = dctPI.Item(piKey)

                        ' if maxCnt of inputs is lower than current
                        If maxCnt < lstInPi.Count Then
                            maxCnt = lstInPi.Count

                            ' add to list of PI with maxCnt
                            maxPI = New ArrayList
                            maxPI.Add(piKey)

                        ElseIf maxCnt = lstInPi.Count Then
                            ' if same cnt with previous, add to list of PI
                            maxPI.Add(piKey)
                        End If
                    Next

                    Dim finalPI As String = ""
                    ' if there is ONE PI, then move to EPI
                    If maxPI.Count = 1 Then
                        finalPI = maxPI.Item(0)

                    ElseIf maxPI.Count > 1 Then
                        ' Choose PI with most "-" (don't care)
                        Dim maxDCCnt As Integer = 0
                        Dim maxDC As String = ""
                        For dcIdx As Integer = 0 To maxPI.Count - 1
                            Dim dc As String = maxPI(dcIdx)
                            Dim dcCnt As Integer = dc.Replace("-", "").Length
                            If dcCnt < maxDCCnt OrElse maxDCCnt = 0 Then
                                ' get the one with less literals
                                maxDCCnt = dcCnt
                                maxDC = dc

                            ElseIf dcCnt = maxDCCnt Then
                                ' if same number of literals.. choose those that has more "1"
                                If dc.Replace("1", "").Length < maxDC.Replace("1", "").Length Then
                                    ' get the one with more "1"
                                    maxDCCnt = dcCnt
                                    maxDC = dc
                                End If



                            End If
                        Next

                        ' PI to be moved to EPI list
                        finalPI = maxDC
                    End If

                    ' finalPI is not blank
                    If finalPI.Length > 0 Then
                        ' update dctInputs?
                        If Not lstEPI.Contains(finalPI) Then
                            lstEPI.Add(finalPI)
                            updateDcts(dctInputs, dctPI, lstEPI)
                        End If
                    End If
                Next

                If lstEPI.Count > maxOutCnt Then
                    maxOutCnt = lstEPI.Count
                End If

                lstAllOutput.Add(lstEPI)
            Next

            dcOutput.Add("sys" & sysCtr, lstAllOutput)
            dcMaxCount.Add("sys" & sysCtr, maxOutCnt)
            dcTimer.Add("sys" & sysCtr, getLstTimer(userCtrl))

        Next

        ' show ladder diagram
        showOutputLadderDiagram(dcOutput, dcMaxCount, dcTimer)

    End Sub

    Private Sub frmSysInput_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        createSysTabPages()
    End Sub

    Private Sub frmSysInput_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Dispose()
    End Sub

#End Region

#Region "Methods"

    Private Function getLstTimer(ByVal userCtrl As ctrlSysInput) As ArrayList
        
        Dim lstTimer As New ArrayList
        If userCtrl.chkTimer.Checked = False Then
            Return lstTimer
        End If

        ' Truth Table of arraylist
        For Each row As DataRow In userCtrl.dtDelay.Rows
            lstTimer.Add(row.Item("DELAY"))
        Next
        Return lstTimer
    End Function

    Private Function isPIcoverINPUT(ByVal input As String, ByVal pi As String) As Boolean
        Dim equalFlg As Boolean = True
        For i As Integer = 0 To pi.Length - 1
            If Not pi.Substring(i, 1).Equals("-") AndAlso Not input.Substring(i, 1).Equals(pi.Substring(i, 1)) Then
                ' pi(bit) <> "-" andAlso pi(bit) <> input(bit)
                equalFlg = False
                Exit For
            End If
        Next

        Return equalFlg
    End Function

    Private Sub createDctInput(ByRef dctInput As Dictionary(Of String, ArrayList), ByVal input As String, ByVal lstPI As ArrayList, ByRef lstEPI As ArrayList)

        Dim lst As New ArrayList

        For Each pi As String In lstPI
            ' is PI covers input?
            If isPIcoverINPUT(input, pi) Then
                lst.Add(pi)
            End If
        Next

        If Not dctInput.ContainsKey(input) Then
            dctInput.Add(input, lst)

        Else
            dctInput.Remove(input)
            dctInput.Add(input, lst)
        End If


        '' if only one PI for an input, add to list of EPIs
        'If lst.Count = 1 AndAlso Not lstEPI.Contains(lst(0)) Then
        '    lstEPI.Add(lst(0))
        'End If
    End Sub

    Private Sub createDctPI(ByRef dctPI As Dictionary(Of String, ArrayList), ByVal pi As String, ByVal lstInput As ArrayList)

        Dim lst As New ArrayList
        For Each input As String In lstInput
            ' is input in PI?
            If isPIcoverINPUT(input, pi) Then
                lst.Add(input)
            End If
        Next

        If Not dctPI.ContainsKey(pi) Then
            dctPI.Add(pi, lst)

        Else
            dctPI.Remove(pi)
            dctPI.Add(pi, lst)
        End If
        ' dctPI.Add(pi, lst)
    End Sub

    Private Sub updateDcts(ByRef dctInput As Dictionary(Of String, ArrayList), ByRef dctPI As Dictionary(Of String, ArrayList), ByVal lstEPI As ArrayList)

        For Each epi As String In lstEPI
            If Not dctPI.ContainsKey(epi) Then
                Continue For
            End If

            ' inputs covered by EPI
            Dim lstInput As ArrayList = dctPI.Item(epi)

            ' remove inputs that are already covered by EPI
            For i As Integer = 0 To lstInput.Count - 1
                dctInput.Remove(lstInput(i))
            Next

            ' remove EPI from dctPI
            dctPI.Remove(epi)
        Next
    End Sub

    Private Sub getEPI(ByRef dctInput As Dictionary(Of String, ArrayList), ByRef lstEPI As ArrayList, ByRef flgAddEPI As Boolean)
        flgAddEPI = False
        For Each input As String In dctInput.Keys
            Dim lstPI As ArrayList = dctInput.Item(input)
            ' if only one PI for an input, add to list of EPIs
            If lstPI.Count = 1 AndAlso Not lstEPI.Contains(lstPI(0)) Then
                lstEPI.Add(lstPI(0))
                flgAddEPI = True
            End If
        Next
    End Sub

    Public Sub showOutputLadderDiagram(ByVal dcOutput As Dictionary(Of String, ArrayList), ByVal dcMaxOutCnt As Dictionary(Of String, Integer), ByVal dcTimer As Dictionary(Of String, ArrayList))
        Dim frmOutput As New frmLadderDiagram
        frmOutput.DcOutputList = dcOutput
        frmOutput.DcTimerList = dcTimer
        frmOutput.DcMaxOutputCount = dcMaxOutCnt

        '' get list of timer values
        'Dim lstTimer As ArrayList = getLstTimer()
        'frmOutput.ListTimer = lstTimer
        'If lstTimer.Count > 0 Then
        '    frmOutput.MaxOutputCount = maxOutCnt + 1
        'End If


        frmOutput.Text = "PROJECT " & """" & mdlMain.projName & """"

        frmOutput.ShowDialog()
    End Sub

    Private Sub getAllSystemInputs()
        For Each tab As TabPage In tabSys.TabPages
            Dim ctrl As ctrlSysInput = tab.Controls(0)

            Debug.Print(tab.Name)
            ' set dictionary of sys to arraylist of rows
            dcLstInput.Item(tab.Name) = ctrl.getInputRows()
        Next
        Debug.Print("getAllSystemInputs DONE")
    End Sub

    Private Sub createSysTabPages()
        ' prepare dictionary of inputs
        dcLstInput = New Dictionary(Of String, ArrayList)

        For i As Integer = 0 To dtSystems.Rows.Count - 1
            ' create user control
            Dim usrSysIn As New ctrlSysInput
            usrSysIn.Dock = DockStyle.Fill
            usrSysIn.Name = "ctrl" & i

            usrSysIn.txtInput.Text = dtSystems.Rows(i).Item("INPUT").ToString
            usrSysIn.txtOutput.Text = dtSystems.Rows(i).Item("OUTPUT").ToString

            Dim pg As TabPage
            If i >= tabSys.TabPages.Count Then
                ' create new tab
                pg = New TabPage
                pg.Name = "sys" & i
                pg.Text = dtSystems.Rows(i).Item("NAME").ToString
                Me.tabSys.TabPages.Add(pg)
            Else
                pg = tabSys.TabPages(i)
                pg.Name = "sys" & i
                pg.Text = dtSystems.Rows(i).Item("NAME").ToString
            End If

            'add page to tab
            tabSys.TabPages("sys" & i).Controls.Add(usrSysIn)

            ' add empty arraylist to dictionary for each system
            dcLstInput.Add("sys" & i, New ArrayList)
        Next
    End Sub

    Private Function combine(ByVal rows As ArrayList, ByVal cntInput As Integer, ByRef lstPI As ArrayList, ByRef isCombine As Boolean) As ArrayList

        'Dim lstPI As New ArrayList ' list of prime implicants
        Dim newLstRows As New ArrayList ' list of uncombined input
        isCombine = False

        Try

            ' if only one row
            If rows.Count = 1 Then
                Return rows
            End If

            ' get combined data only...
            For Each row1 As String In rows
                Dim isRowPI As Boolean = False

                ' check with rows
                For Each row2 As String In rows

                    ' if same row, ignore
                    If row1.Equals(row2) Then
                        Continue For
                    End If

                    Dim cntDiff As Integer = 0
                    Dim combinedRow As String = ""

                    ' check each bit of each row if equal
                    For i As Integer = 0 To cntInput - 1
                        ' if row1(bit) <> row2(bit), then it is "DON'T CARE"
                        If Not row1.Substring(i, 1).Equals(row2.Substring(i, 1)) Then
                            cntDiff = cntDiff + 1
                            combinedRow = combinedRow & "-"

                        Else
                            ' if row1(bit) = row2(bit), get value
                            combinedRow = combinedRow & row2.Substring(i, 1)
                        End If
                    Next

                    ' if two rows are diff in only one bit, save as Prime Implicant
                    If cntDiff = 1 Then
                        If Not lstPI.Contains(combinedRow) Then
                            lstPI.Add(combinedRow)
                        End If

                        isCombine = True
                        isRowPI = True
                    End If
                Next

                If isRowPI = False Then
                    newLstRows.Add(row1)
                End If

            Next

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try

        Return newLstRows

    End Function

#End Region

End Class