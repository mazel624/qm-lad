﻿Public Class DgvLadderDiagram

    Public Const CELL_W As Integer = 100
    Public Const CELL_H As Integer = 100

    Public mapCanvas As Dictionary(Of ITEM_POS, Bitmap)

    Public fontLadSymLabel As New Font("MS UI Gothic", 12)
    Public rectLadSymLabel As New RectangleF(30, 25, 45, 14)

    Public fontTimerLabel As New Font("MS UI Gothic", 10)
    Public rectTimerChip As New RectangleF(30, 25, 45, 14) ' timer chip label
    Public rectTimerTL As New RectangleF(5, 50, 20, 12)  'top-left : IN
    Public rectTimerTR As New RectangleF(60, 50, 35, 12)  'top-right : TON
    Public rectTimerBL As New RectangleF(5, 80, 45, 12)  'bottom-left : PT
    Public rectTimerBR As New RectangleF(45, 80, 50, 12)  'bottom-right : 100ms
    Public rectTimer As New RectangleF(40, 80, 50, 12)  'input time

    Private sfL As StringFormat
    Private sfR As StringFormat

    Private sf As StringFormat
    Private maxOutCnt As Integer
    Private lstOutput As ArrayList
    Private lstTimer As ArrayList ' >> arraylist of time.. if value is 0, no timer needed... if * at the end, dependency EXISTS



    Private lstInputLabel() As String = {"I 0.0", "I 0.1", "I 0.2", "I 0.3", "I 0.4", "I 0.5", "I 0.6", "I 0.7"}
    Private lstOutputLabel() As String = {"Q 0.0", "Q 0.1", "Q 0.2", "Q 0.3", "Q 0.4", "Q 0.5"}
    'Private lstTimerLabel() As String = {"T 37", "T 38", "T 39", "T 40", "T 41", "T 42"}

    Public WriteOnly Property ListOutput() As ArrayList
        Set(ByVal value As ArrayList)
            lstOutput = value
        End Set
    End Property

    Public Property ListTimer() As ArrayList
        Get
            Return lstTimer
        End Get
        Set(ByVal value As ArrayList)
            lstTimer = value
        End Set
    End Property

    Public Property MaxOutputCount() As Integer
        Get
            Return maxOutCnt
        End Get
        Set(ByVal value As Integer)
            maxOutCnt = value
        End Set
    End Property

    Public Enum ITEM_POS
        TopCell
        MidCell
        BottomCell
        TopOnlyCell
        StartCell
        FillerCell
        EndCell
        InputRungCell
        OutputRungCell
        OutputCell
        TimerCellL
        TimerCellR
        FillerTimerCell
    End Enum

    Public Enum LAD_SYM
        NO 'Normally open     -| |-    A'=0
        NC 'Normally closed   -|/|-    A =1
        OUT
    End Enum

    Private Sub initTemplate()
        ' createTemplateCanvas
        mapCanvas = New Dictionary(Of ITEM_POS, Bitmap)
        mapCanvas.Add(ITEM_POS.InputRungCell, getLineTemplate(ITEM_POS.InputRungCell))
        mapCanvas.Add(ITEM_POS.OutputRungCell, getLineTemplate(ITEM_POS.OutputRungCell))
        mapCanvas.Add(ITEM_POS.StartCell, getLineTemplate(ITEM_POS.StartCell))
        mapCanvas.Add(ITEM_POS.EndCell, getLineTemplate(ITEM_POS.EndCell))
        mapCanvas.Add(ITEM_POS.FillerCell, getLineTemplate(ITEM_POS.FillerCell))
        mapCanvas.Add(ITEM_POS.TopOnlyCell, getLadSymTemplate(LAD_SYM.NO, ITEM_POS.TopOnlyCell))
        mapCanvas.Add(ITEM_POS.TopCell, getLadSymTemplate(LAD_SYM.NO, ITEM_POS.TopCell))
        mapCanvas.Add(ITEM_POS.MidCell, getLadSymTemplate(LAD_SYM.NO, ITEM_POS.MidCell))
        mapCanvas.Add(ITEM_POS.BottomCell, getLadSymTemplate(LAD_SYM.NO, ITEM_POS.BottomCell))
        mapCanvas.Add(ITEM_POS.OutputCell, getLadSymTemplate(LAD_SYM.OUT, ITEM_POS.OutputCell))
        'mapCanvas.Add(ITEM_POS.OutputCell, getTimerTemplate(ITEM_POS.TimerCellL))
        mapCanvas.Add(ITEM_POS.TimerCellL, getTimerTemplate(ITEM_POS.TimerCellL))
        mapCanvas.Add(ITEM_POS.TimerCellR, getTimerTemplate(ITEM_POS.TimerCellR))
        mapCanvas.Add(ITEM_POS.FillerTimerCell, getLineTemplate(ITEM_POS.FillerTimerCell))

        sf = New StringFormat
        sf.Alignment = StringAlignment.Center
        'sf.LineAlignment = StringAlignment.Center

        sfL = New StringFormat
        sfL.Alignment = StringAlignment.Near

        sfR = New StringFormat
        sfR.Alignment = StringAlignment.Far
    End Sub


    Public Sub start()

        'Initialize Template
        initTemplate()

        'Disable Selection
        Me.dgv.DefaultCellStyle.SelectionBackColor = Me.dgv.DefaultCellStyle.BackColor
        Me.dgv.DefaultCellStyle.SelectionForeColor = Me.dgv.DefaultCellStyle.ForeColor


        ' create test data
        'test1()

        'dgvTemplate.RowTemplate.Height = CELL_H

        ' update number of columns
        dgv.Columns.Add("START", "START")
        dgv.Columns("START").Width = CELL_W
        For i As Integer = 0 To maxOutCnt  '>>> last column is for output
            dgv.Columns.Add("COL" & i, "COL" & i)
            dgv.Columns("COL" & i).Width = CELL_W
        Next
        ' additional column for timer
        If lstTimer.Count > 0 Then
            dgv.Columns.Add("COL" & maxOutCnt + 1, "COL" & maxOutCnt + 1)
            dgv.Columns("COL" & maxOutCnt + 1).Width = CELL_W
        End If
        'dgvTemplate.Columns.Add("OUT", "OUT")
        'dgvTemplate.Columns.Add("END", "END")

        ' FIRST ROW
        dgv.Rows.Add()
        dgv.Rows(0).Cells("START") = getLineCell(ITEM_POS.InputRungCell)
        'dgvTemplate.Rows(0).Cells("END") = getLineCell(ITEM_POS.OutputRungCell)
        dgv.Rows(0).Height = 30

        Dim rowIdx As Integer = dgv.Rows.Count - 1
        For i As Integer = 0 To lstOutput.Count - 1
            dgv.Rows.Add()
            rowIdx = dgv.Rows.Count - 1
            dgv.Rows(rowIdx).Cells("START") = getLineCell(ITEM_POS.StartCell)
            'dgvTemplate.Rows(rowIdx).Cells("END") = getLineCell(ITEM_POS.EndCell)
            dgv.Rows(rowIdx).Height = CELL_H

            Dim timerLabel As String = ""
            Dim time As String = ""
            If lstTimer.Count > 0 Then
                timerLabel = "T 37" 'lstTimerLabel(i)
                time = lstTimer(i)
            End If

            Dim lstPI As ArrayList = lstOutput.Item(i)
            createOutput(lstPI, lstOutputLabel(i), timerLabel, time)

            ' if timer is set, additional row
            If lstTimer.Count > 0 Then
                Dim timer As String = lstTimer(i)

                If timer.Length > 0 Then
                    dgv.Rows.Add()
                    rowIdx = dgv.Rows.Count - 1
                    dgv.Rows(rowIdx).Cells("START") = getLineCell(ITEM_POS.InputRungCell)
                    'dgvTemplate.Rows(rowIdx).Cells("END") = getLineCell(ITEM_POS.OutputRungCell)
                    dgv.Rows(rowIdx).Height = CELL_H
                End If
            End If

            dgv.Rows.Add()
            rowIdx = dgv.Rows.Count - 1
            dgv.Rows(rowIdx).Cells("START") = getLineCell(ITEM_POS.InputRungCell)
            'dgvTemplate.Rows(rowIdx).Cells("END") = getLineCell(ITEM_POS.OutputRungCell)
            dgv.Rows(rowIdx).Height = 30


            Debug.Print("ROW IDX : " & rowIdx)
            Debug.Print("ROW COUNT : " & dgv.Rows.Count)
        Next
    End Sub



    Private Sub test1()
        lstOutput = New ArrayList

        ' input will be list of PI >> e.g.  { 1--0--, 00-11- }
        Dim lstPI1 As New ArrayList
        lstPI1.Add("1-0-")  '>> COL1
        lstPI1.Add("-0-1")  '>> COL2
        'lstPI1.Add("")  '>> FILLER

        Dim lstPI2 As New ArrayList
        lstPI2.Add("00-1")  '>> COL1
        lstPI2.Add("11-0")  '>> COL2
        lstPI2.Add("-111")  '>> COL3

        Dim lstPI3 As New ArrayList
        lstPI3.Add("1-0-")  '>> COL1
        lstPI3.Add("-0-1")  '>> COL2
        lstPI3.Add("1001")  '>> COL3

        Dim lstPI4 As New ArrayList
        lstPI4.Add("00-1")  '>> COL1
        lstPI4.Add("11-0")  '>> COL2
        lstPI4.Add("-1-0")  '>> COL3
        lstPI4.Add("1001")  '>> COL4
        lstPI4.Add("1-1-")  '>> COL4

        Dim lstPI5 As New ArrayList
        lstPI5.Add("1---")  '>> COL3

        lstOutput.Add(lstPI1)
        lstOutput.Add(lstPI2)
        lstOutput.Add(lstPI3)
        lstOutput.Add(lstPI4)
        lstOutput.Add(lstPI5)
        lstOutput.Add(lstPI3)

        maxOutCnt = 6  ' number of output >>> columns 


        'timer
        lstTimer = New ArrayList
        lstTimer.Add("5")
        lstTimer.Add("")
        lstTimer.Add("25")
        lstTimer.Add("")
        lstTimer.Add("45")
        lstTimer.Add("")
    End Sub

    Private Sub createOutput(ByVal lstPI As ArrayList, ByVal outLabel As String, ByVal timerLabel As String, ByVal time As String)

        'index of first row
        Dim startRow As Integer = dgv.Rows.Count - 1
        Dim isEnd As Boolean = False
        For colIdx As Integer = 0 To maxOutCnt - 1
            Dim rowIdx As Integer = startRow

            ' if colIdx is more than actual count of output, just put fillers
            If colIdx >= lstPI.Count Then
                ''filler
                'dgvTemplate.Rows(rowIdx).Cells("COL" & colIdx) = getLineCell(ITEM_POS.FillerCell)
                'Continue For

                If lstTimer.Count = 0 OrElse time.Length = 0 OrElse time.Equals("0") Then
                    ' print output and exit for loop
                    dgv.Rows(startRow).Cells("COL" & colIdx) = getLadOutSymCell(outLabel, sf)

                Else
                    ' if there is timer
                    dgv.Rows(startRow).Cells("COL" & colIdx) = getLineCell(ITEM_POS.FillerTimerCell, time)
                    dgv.Rows(startRow).Cells("COL" & colIdx + 1) = getTimerCell(timerLabel)

                    ' add rows if not enough
                    If startRow + 1 = dgv.Rows.Count Then
                        dgv.Rows.Add()
                    End If

                    ' draw START_RUNG cell
                    dgv.Rows(startRow + 1).Cells("START") = getLineCell(ITEM_POS.InputRungCell)
                    dgv.Rows(startRow + 1).Cells("COL" & colIdx) = getLadSymTimerCell(timerLabel, sf)
                    dgv.Rows(startRow + 1).Cells("COL" & colIdx + 1) = getLadOutSymCell(outLabel, sf)

                End If

                isEnd = True
                Exit For
            End If

            ' get output(PI)
            Dim pi As String = lstPI.Item(colIdx)

            ' if empty PI
            If pi.Length = 0 Then
                'dgvTemplate.Rows(rowIdx).Cells("COL" & colIdx) = getLineCell(ITEM_POS.FillerCell)
                'Continue For

                ' print output and exit for loop
                If lstTimer.Count = 0 Then
                    ' print output and exit for loop
                    dgv.Rows(startRow).Cells("COL" & colIdx) = getLadOutSymCell(outLabel, sf)

                Else
                    ' if there is timer
                    dgv.Rows(startRow + 1).Cells("COL" & colIdx) = getLadOutSymCell(outLabel, sf)

                End If

                isEnd = True
                Exit For
            End If

            'flag for symbols on the top 
            Dim isTop As Boolean = False

            ' get last index of symbol (not "-")
            Dim lastIdx As Integer = pi.LastIndexOf("0")
            If pi.LastIndexOf("1") > lastIdx Then
                lastIdx = pi.LastIndexOf("1")
            End If

            ' loop through each bit of the PI
            For idx As Integer = 0 To lastIdx

                ' if "-", don't care (no symbol)
                If pi.Substring(idx, 1).Equals("-") Then
                    Continue For
                End If

                ' get Position of cell
                Dim itemPos As ITEM_POS
                If isTop Then
                    ' TopCell already created and output has more than 1 level... set to midcell
                    itemPos = ITEM_POS.MidCell

                    ' if symbol is already the last
                    If lastIdx = idx Then
                        itemPos = ITEM_POS.BottomCell
                    End If

                    ' add one row
                    rowIdx = rowIdx + 1

                    ' add rows if not enough
                    If rowIdx >= dgv.Rows.Count Then
                        dgv.Rows.Add()
                        rowIdx = dgv.Rows.Count - 1
                    End If

                    ' draw START_RUNG and END_RUNG cells
                    dgv.Rows(rowIdx).Cells("START") = getLineCell(ITEM_POS.InputRungCell)
                    'dgvTemplate.Rows(rowIdx).Cells("END") = getLineCell(ITEM_POS.OutputRungCell)

                Else
                    ' TopCell not yet created
                    itemPos = ITEM_POS.TopCell

                    ' if current index is already the last...
                    If lastIdx = idx Then
                        itemPos = ITEM_POS.TopOnlyCell
                    End If

                    isTop = True
                End If

                'get ladder symbol
                Dim ladSym As LAD_SYM = LAD_SYM.NO
                If pi.Substring(idx, 1).Equals("1") Then
                    ' NC
                    ladSym = LAD_SYM.NC
                End If

                ' get cell of ladder diagram symbol
                dgv.Rows(rowIdx).Cells("COL" & colIdx) = getLadSymCell(ladSym, itemPos, lstInputLabel(idx), sf)

            Next

        Next

        ' output is not yet displayed
        If Not isEnd Then
            If lstTimer.Count = 0 Then
                ' print output and exit for loop
                dgv.Rows(startRow).Cells("COL" & maxOutCnt) = getLadOutSymCell(outLabel, sf)

            Else
                ' if there is timer
                dgv.Rows(startRow + 1).Cells("COL" & maxOutCnt) = getLadOutSymCell(outLabel, sf)

            End If
            'dgvTemplate.Rows(startRow).Cells("COL" & maxOutCnt) = getLadOutSymCell(outLabel, sf)
        End If
        ' set end of of rung
        'dgvTemplate.Rows(startRow).Cells("END") = getLineCell(ITEM_POS.EndCell)

    End Sub

    ' -||-
    Private Function getStyleNO(ByVal itemPos As ITEM_POS) As ArrayList
        Dim lst As New ArrayList

        ' (L) -
        ' x,y
        Dim ps As Point() = { _
           New Point(25, 60), _
           New Point(40, 60) _
        }

        ' (L) |
        Dim ps1 As Point() = { _
            New Point(40, 50), _
            New Point(40, 70) _
        }

        ' (R) |
        Dim ps2 As Point() = { _
            New Point(60, 50), _
            New Point(60, 70) _
                }

        ' (R) -
        Dim ps3 As Point() = { _
            New Point(60, 60), _
            New Point(75, 60) _
        }

        lst.Add(ps)
        lst.Add(ps1)
        lst.Add(ps2)
        lst.Add(ps3)

        lst.AddRange(getLine(itemPos))

        Return lst
    End Function

    ' -|/|-
    Private Function getStyleNC(ByVal itemPos As ITEM_POS) As ArrayList
        Dim lst As New ArrayList

        ' get NO
        lst = getStyleNO(itemPos)

        ' /
        ' x,y
        Dim ps As Point() = { _
           New Point(60, 50), _
           New Point(40, 70) _
        }

        lst.Add(ps)

        Return lst
    End Function

    ' -()-
    Private Function getStyleOUT(ByVal itemPos As ITEM_POS) As ArrayList
        Dim lst As New ArrayList

        ' (L) -
        ' x,y
        Dim ps As Point() = { _
           New Point(0, 60), _
           New Point(40, 60) _
        }

        lst.Add(ps)

        Return lst
    End Function

    ' Timer box
    Private Function getStyleTimer(ByVal itemPos As ITEM_POS) As ArrayList
        Dim lst As New ArrayList

        If itemPos = ITEM_POS.TimerCellL Then
            ' get box
            Dim ps As Point() = { _
               New Point(99, 40), _
               New Point(0, 40), _
               New Point(0, 99), _
               New Point(99, 99), _
               New Point(99, 40) _
            }

            lst.Add(ps)

        Else
            lst = getStyleNC(ITEM_POS.TimerCellR)

        End If


        Return lst
    End Function

    ' |
    Private Function getLine(ByVal itemPos As ITEM_POS) As ArrayList

        Dim lst As New ArrayList

        Select Case itemPos

            Case ITEM_POS.TopOnlyCell

                ' LEFT SIDE
                Dim ps1 As Point() = { _
                    New Point(0, 60), _
                    New Point(25, 60) _
                }

                ' RIGHT SIDE
                Dim ps2 As Point() = { _
                    New Point(75, 60), _
                    New Point(100, 60) _
                }

                lst.Add(ps1)
                lst.Add(ps2)

            Case ITEM_POS.TopCell

                ' LEFT SIDE
                Dim ps1 As Point() = { _
                    New Point(0, 60), _
                    New Point(25, 60) _
                }

                Dim ps2 As Point() = { _
                    New Point(25, 60), _
                    New Point(25, 100) _
                }

                ' RIGHT SIDE
                Dim ps3 As Point() = { _
                    New Point(75, 60), _
                    New Point(100, 60) _
                }

                Dim ps4 As Point() = { _
                    New Point(75, 60), _
                    New Point(75, 100) _
                }

                lst.Add(ps1)
                lst.Add(ps2)
                lst.Add(ps3)
                lst.Add(ps4)

            Case ITEM_POS.MidCell

                ' LEFT SIDE
                Dim ps1 As Point() = { _
                    New Point(25, 0), _
                    New Point(25, 100) _
                }

                Dim ps2 As Point() = { _
                    New Point(25, 60), _
                    New Point(40, 60) _
                }

                ' RIGHT SIDE
                Dim ps3 As Point() = { _
                    New Point(75, 0), _
                    New Point(75, 100) _
                }

                Dim ps4 As Point() = { _
                    New Point(60, 60), _
                    New Point(75, 60) _
                }

                lst.Add(ps1)
                lst.Add(ps2)
                lst.Add(ps3)
                lst.Add(ps4)

            Case ITEM_POS.BottomCell

                ' LEFT SIDE
                Dim ps1 As Point() = { _
                    New Point(25, 0), _
                    New Point(25, 60) _
                }

                Dim ps2 As Point() = { _
                    New Point(25, 60), _
                    New Point(40, 60) _
                }

                ' RIGHT SIDE
                Dim ps3 As Point() = { _
                    New Point(75, 0), _
                    New Point(75, 60) _
                }

                Dim ps4 As Point() = { _
                    New Point(60, 60), _
                    New Point(75, 60) _
                }

                lst.Add(ps1)
                lst.Add(ps2)
                lst.Add(ps3)
                lst.Add(ps4)

            Case ITEM_POS.InputRungCell ' | (LEFT)
                ' Vertical
                Dim ps As Point() = { _
                    New Point(70, 0), _
                    New Point(70, 100) _
                }

                lst.Add(ps)

            Case ITEM_POS.OutputRungCell  ' | (RIGHT)
                ' VERTICAL
                Dim ps As Point() = { _
                   New Point(30, 0), _
                   New Point(30, 100) _
                }

                lst.Add(ps)

            Case ITEM_POS.StartCell ' |--
                ' Horizontal
                Dim ps1 As Point() = { _
                    New Point(70, 60), _
                    New Point(100, 60) _
                }

                ' Vertical
                Dim ps2 As Point() = { _
                    New Point(70, 0), _
                    New Point(70, 100) _
                }

                lst.Add(ps1)
                lst.Add(ps2)

            Case ITEM_POS.EndCell  ' --|
                Dim ps1 As Point() = { _
                    New Point(0, 60), _
                    New Point(30, 60) _
                }

                Dim ps2 As Point() = { _
                   New Point(30, 0), _
                   New Point(30, 100) _
                }

                lst.Add(ps1)
                lst.Add(ps2)

            Case ITEM_POS.TimerCellR    ' --|
                ' LEFT SIDE
                Dim ps1 As Point() = { _
                    New Point(25, 0), _
                    New Point(25, 60) _
                }

                Dim ps2 As Point() = { _
                    New Point(25, 60), _
                    New Point(40, 60) _
                }


                'RIGHT SIDE
                Dim ps3 As Point() = { _
                    New Point(75, 60), _
                    New Point(100, 60) _
                }

                lst.Add(ps1)
                lst.Add(ps2)
                lst.Add(ps3)

            Case ITEM_POS.FillerTimerCell   ' --|
                'Horizontal
                Dim ps As Point() = { _
                    New Point(0, 60), _
                    New Point(100, 60) _
                }

                'Vertical
                Dim ps1 As Point() = { _
                    New Point(25, 60), _
                    New Point(25, 100) _
                }

                ' for label time
                Dim ps3 As Point() = { _
                   New Point(90, 85), _
                   New Point(100, 85) _
                }

                lst.Add(ps)
                lst.Add(ps1)
                lst.Add(ps3)

            Case Else 'Filler Cell  ----
                Dim ps1 As Point() = { _
                    New Point(0, 60), _
                    New Point(100, 60) _
                }

                lst.Add(ps1)

        End Select

        Return lst
    End Function

    Private Function getLineCell(ByVal itemPos As ITEM_POS, Optional ByVal time As String = "") As DataGridViewImageCell

        Dim cell As New DataGridViewImageCell

        Try
            ' get canvas from map
            Dim canvas As New Bitmap(CELL_W, CELL_H)
            canvas = mapCanvas.Item(itemPos).Clone

            If itemPos = ITEM_POS.FillerTimerCell Then
                'time
                Dim g As Graphics = Graphics.FromImage(canvas)
                g.FillRectangle(Brushes.White, rectTimer)
                g.DrawString(time, fontTimerLabel, Brushes.DarkCyan, rectTimer, sfR)

                g.Dispose()
            End If

            cell.Value = canvas

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try

        Return cell

    End Function

    Private Function getLadSymCell(ByVal sym As LAD_SYM, ByVal itemPos As ITEM_POS, ByVal label As String, _
                                ByVal sf As StringFormat) As DataGridViewImageCell

        Dim cell As New DataGridViewImageCell
        Try
            Dim canvas As New Bitmap(CELL_W, CELL_H)
            canvas = mapCanvas.Item(itemPos).Clone()
            Dim g As Graphics = Graphics.FromImage(canvas)

            ' Ladder Symbol
            If sym = LAD_SYM.NC Then
                ' -|/|-   draw diagonal line
                Dim lstStyle As ArrayList = getStyleNC(itemPos)

                For Each style() As Point In lstStyle
                    g.DrawLines(Pens.Black, style)
                Next
            End If

            'Label
            g.FillRectangle(Brushes.White, rectLadSymLabel)
            g.DrawString(label, fontLadSymLabel, Brushes.DarkCyan, rectLadSymLabel, sf)

            g.Dispose()

            cell.Value = canvas

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try

        Return cell

    End Function

    Private Function getLadOutSymCell(ByVal label As String, _
                                ByVal sf As StringFormat) As DataGridViewImageCell

        Dim cell As New DataGridViewImageCell
        Try
            Dim canvas As New Bitmap(CELL_W, CELL_H)
            canvas = mapCanvas.Item(ITEM_POS.OutputCell).Clone()
            Dim g As Graphics = Graphics.FromImage(canvas)

            'label
            g.FillRectangle(Brushes.White, rectLadSymLabel)
            g.DrawString(label, fontLadSymLabel, Brushes.DarkCyan, rectLadSymLabel, sf)

            g.Dispose()

            cell.Value = canvas

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try

        Return cell

    End Function

    Private Function getLadSymTimerCell(ByVal label As String, _
                                ByVal sf As StringFormat) As DataGridViewImageCell

        Dim cell As New DataGridViewImageCell
        Try
            Dim canvas As New Bitmap(CELL_W, CELL_H)
            canvas = mapCanvas.Item(ITEM_POS.TimerCellR).Clone()
            Dim g As Graphics = Graphics.FromImage(canvas)

            'Label
            g.FillRectangle(Brushes.White, rectLadSymLabel)
            g.DrawString(label, fontLadSymLabel, Brushes.DarkCyan, rectLadSymLabel, sf)

            g.Dispose()

            cell.Value = canvas

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try

        Return cell

    End Function

    Private Function getTimerCell(ByVal labelChip As String) As DataGridViewImageCell

        Dim cell As New DataGridViewImageCell
        Try
            Dim canvas As New Bitmap(CELL_W, CELL_H)
            canvas = mapCanvas.Item(ITEM_POS.TimerCellL).Clone()
            Dim g As Graphics = Graphics.FromImage(canvas)

            'label
            g.FillRectangle(Brushes.White, rectTimerChip)
            g.DrawString(labelChip, fontTimerLabel, Brushes.DarkCyan, rectTimerChip, sf)

            'label inside timer
            'top-left
            g.FillRectangle(Brushes.White, rectTimerTL)
            g.DrawString("IN", fontTimerLabel, Brushes.DarkCyan, rectTimerTL, sfL)
            'top-right
            g.FillRectangle(Brushes.White, rectTimerTR)
            g.DrawString("TON", fontTimerLabel, Brushes.DarkCyan, rectTimerTR, sfR)
            'bottom-left
            g.FillRectangle(Brushes.White, rectTimerBL)
            g.DrawString("PT", fontTimerLabel, Brushes.DarkCyan, rectTimerBL, sfL)
            'bottom-right
            g.FillRectangle(Brushes.White, rectTimerBR)
            g.DrawString("100 ms", fontTimerLabel, Brushes.DarkCyan, rectTimerBR, sfR)

            g.Dispose()

            cell.Value = canvas

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try

        Return cell

    End Function

    Private Function getLineTemplate(ByVal type As ITEM_POS) As Bitmap
        Dim canvas As New Bitmap(CELL_W, CELL_H)

        Try
            ' Style
            Dim lstStyle As ArrayList = getLine(type)

            Dim g As Graphics = Graphics.FromImage(canvas)

            For Each style() As Point In lstStyle
                g.DrawLines(Pens.Green, style)
            Next

            g.Dispose()

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try

        Return canvas

    End Function

    Private Function getLadSymTemplate(ByVal sym As LAD_SYM, ByVal itemPos As ITEM_POS) As Bitmap

        Dim lstStyle As ArrayList
        Dim canvas As New Bitmap(CELL_W, CELL_H)

        Try
            ' Style
            If sym = LAD_SYM.NC Then
                lstStyle = getStyleNC(itemPos)
            ElseIf sym = LAD_SYM.NO Then
                lstStyle = getStyleNO(itemPos)
            Else
                lstStyle = getStyleOUT(itemPos)
            End If


            Dim g As Graphics = Graphics.FromImage(canvas)

            For Each style() As Point In lstStyle
                g.DrawLines(Pens.Black, style)
            Next

            If sym = LAD_SYM.OUT Then
                ' Ladder Symbol  (  )
                g.DrawArc(Pens.Black, 40, 50, 10, 20, 270, -180)
                g.DrawArc(Pens.Black, 50, 50, 10, 20, 270, 180)
            End If

            g.Dispose()

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try

        Return canvas

    End Function

    Private Function getTimerTemplate(ByVal itemPos As ITEM_POS) As Bitmap

        Dim lstStyle As ArrayList
        Dim canvas As New Bitmap(CELL_W, CELL_H)

        Try
            ' Style Timer
            lstStyle = getStyleTimer(itemPos)

            Dim g As Graphics = Graphics.FromImage(canvas)

            For Each style() As Point In lstStyle
                g.DrawLines(Pens.Black, style)
            Next

            g.Dispose()

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try

        Return canvas

    End Function
End Class
