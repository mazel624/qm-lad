﻿Public Class ctrlSysInput

    Public cntInput As Integer
    Public cntOutput As Integer
    Public dtIn As DataTable
    Public dtDelay As DataTable

    Public lstInputRows As ArrayList

    Private Sub chkTimer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTimer.CheckedChanged
        dgvTimer.Enabled = chkTimer.Checked
        dgvTimer.ReadOnly = Not chkTimer.Checked
        dgvTimer.ClearSelection()
    End Sub

    Private Sub SysInput_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cntInput = convertString2Int(txtInput.Text)
        cntOutput = convertString2Int(txtOutput.Text)

        initDgvTruthTable()
        initDgvDelay()
    End Sub

    Public Sub initDtDelay()
        dtDelay = New DataTable
        dtDelay.Columns.Add("NO")
        dtDelay.Columns.Add("DELAY")
    End Sub

    Public Sub initDtIn()
        dtIn = New DataTable

        For i As Integer = 1 To cntInput
            dtIn.Columns.Add("I" & i)
        Next

        For i As Integer = 1 To cntOutput
            dtIn.Columns.Add("O" & i)
        Next
    End Sub

    Public Sub initDgvDelay()
        Try
            dgvTimer.DataSource = Nothing
            initDtDelay()

            dgvTimer.DataSource = dtDelay
            dgvTimer.Columns(0).DataPropertyName = "NO"
            dgvTimer.Columns(1).DataPropertyName = "DELAY"

            For i As Integer = 1 To cntOutput
                dtDelay.Rows.Add(i.ToString, "0")
            Next

            ' reset datasource of datagridview

            refreshDGVTimer()

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try
    End Sub

    Public Sub initDgvTruthTable()
        Dim idxI As Integer = 0  ' header index
        Dim idxO As Integer = 0  ' header index
        Dim plcCtrI As Integer = 0
        Dim plcCtrO As Integer = 0
        Dim headerText As String = ""  ' header label

        Try

            ' reset datasource of datagridview
            dgvTruthTable.DataSource = Nothing

            ' initialize dtIn
            initDtIn()


            For Each col As DataColumn In dtIn.Columns

                If col.ColumnName.StartsWith("I") Then
                    ' input column

                    ' get header text
                    headerText = mdlFv.IN_HEADER(idxI).Replace("I 0", "I " & plcCtrI)

                    ' create datagridview column
                    Dim dgvCol As New DataGridViewTextBoxColumn
                    With dgvCol
                        .Name = col.ColumnName
                        .HeaderText = headerText
                        .DataPropertyName = col.ColumnName
                        .ReadOnly = True
                        .Width = 50
                        .SortMode = DataGridViewColumnSortMode.NotSortable
                    End With

                    ' add dgvCol to datagridviewTable
                    dgvTruthTable.Columns.Add(dgvCol)


                    ' update header index
                    idxI += 1
                    If idxI = mdlFv.IN_HEADER.Length Then
                        idxI = 0
                        plcCtrI += 1
                    End If

                Else
                    ' output column 

                    ' get header text
                    headerText = mdlFv.OUT_HEADER(idxO).Replace("Q 0", "Q " & plcCtrO)

                    ' create datagridview column
                    Dim dgvCol As New DataGridViewCheckBoxColumn
                    With dgvCol
                        .Name = col.ColumnName
                        .HeaderText = headerText
                        .DataPropertyName = col.ColumnName
                        .ReadOnly = False
                        .Width = 50
                        .SortMode = DataGridViewColumnSortMode.NotSortable
                        .TrueValue = "1"
                        .IndeterminateValue = ""
                        .FalseValue = "0"
                    End With
                    ' add dgvCol to datagridviewTable
                    dgvTruthTable.Columns.Add(dgvCol)

                    idxO += 1
                    If idxO = mdlFv.OUT_HEADER.Length Then
                        idxO = 0
                        plcCtrO += 1
                    End If
                End If

            Next

            ' set datasource of datagridview
            dgvTruthTable.DataSource = dtIn

            ' create truth table
            createTruthTable(cntInput, cntOutput)

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try
    End Sub

    Public Sub createTruthTable(ByVal cntInput As Integer, ByVal cntOutput As Integer)
        ' # of rows is 2^cntInput
        Dim rowCount As Long = 2 ^ cntInput

        ' create truth table
        For rowIdx As Integer = 0 To rowCount - 1
            Dim binary As String = Convert.ToString(rowIdx, 2)
            binary = binary.PadLeft(cntInput, "0"c)

            ' set binary to individual cells
            Dim data As New ArrayList
            For colIdx As Integer = 0 To binary.Length - 1
                data.Add(binary.Substring(colIdx, 1))
            Next

            dtIn.Rows.Add(data.ToArray)
        Next

        refreshDGVTruthTable()
    End Sub

    Public Sub refreshDGVTruthTable()
        dgvTruthTable.ClearSelection()
        dgvTruthTable.Refresh()
    End Sub

    Public Sub refreshDGVTimer()
        dgvTimer.ClearSelection()
        dgvTimer.Refresh()
    End Sub

    Public Function getInputRows() As ArrayList
        Try
            lstInputRows = New ArrayList

            ' get inputs of those that has a "1" in output
            For i As Integer = 0 To cntOutput - 1
                Dim lstRows As New ArrayList
                Dim rows() As DataRow = dtIn.Select(dtIn.Columns(cntInput + i).ColumnName & " = '1'")

                ' join string
                For Each row As DataRow In rows
                    lstRows.Add(String.Concat(row.ItemArray).Substring(0, cntInput))
                Next

                lstInputRows.Add(lstRows)
            Next

        Catch ex As Exception
            Debug.Print(ex.StackTrace)
        End Try

        Return lstInputRows

    End Function

End Class
