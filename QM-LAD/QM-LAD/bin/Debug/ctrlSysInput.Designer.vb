﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ctrlSysInput
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblnput = New System.Windows.Forms.Label
        Me.txtInput = New System.Windows.Forms.TextBox
        Me.txtOutput = New System.Windows.Forms.TextBox
        Me.lblOutput = New System.Windows.Forms.Label
        Me.lblTruthTable = New System.Windows.Forms.Label
        Me.dgvTruthTable = New System.Windows.Forms.DataGridView
        Me.dgvTimer = New System.Windows.Forms.DataGridView
        Me.chkTimer = New System.Windows.Forms.CheckBox
        Me.colNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colTimer = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvTruthTable, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblnput
        '
        Me.lblnput.AutoSize = True
        Me.lblnput.Location = New System.Drawing.Point(21, 15)
        Me.lblnput.Name = "lblnput"
        Me.lblnput.Size = New System.Drawing.Size(64, 12)
        Me.lblnput.TabIndex = 23
        Me.lblnput.Text = "No. of Input"
        '
        'txtInput
        '
        Me.txtInput.Location = New System.Drawing.Point(91, 12)
        Me.txtInput.Name = "txtInput"
        Me.txtInput.ReadOnly = True
        Me.txtInput.Size = New System.Drawing.Size(70, 19)
        Me.txtInput.TabIndex = 24
        '
        'txtOutput
        '
        Me.txtOutput.Location = New System.Drawing.Point(91, 35)
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ReadOnly = True
        Me.txtOutput.Size = New System.Drawing.Size(70, 19)
        Me.txtOutput.TabIndex = 26
        '
        'lblOutput
        '
        Me.lblOutput.AutoSize = True
        Me.lblOutput.Location = New System.Drawing.Point(12, 38)
        Me.lblOutput.Name = "lblOutput"
        Me.lblOutput.Size = New System.Drawing.Size(73, 12)
        Me.lblOutput.TabIndex = 25
        Me.lblOutput.Text = "No. of Output"
        '
        'lblTruthTable
        '
        Me.lblTruthTable.AutoSize = True
        Me.lblTruthTable.Location = New System.Drawing.Point(11, 67)
        Me.lblTruthTable.Name = "lblTruthTable"
        Me.lblTruthTable.Size = New System.Drawing.Size(64, 12)
        Me.lblTruthTable.TabIndex = 27
        Me.lblTruthTable.Text = "Truth Table"
        '
        'dgvTruthTable
        '
        Me.dgvTruthTable.AllowUserToAddRows = False
        Me.dgvTruthTable.AllowUserToDeleteRows = False
        Me.dgvTruthTable.AllowUserToResizeColumns = False
        Me.dgvTruthTable.AllowUserToResizeRows = False
        Me.dgvTruthTable.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvTruthTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvTruthTable.EnableHeadersVisualStyles = False
        Me.dgvTruthTable.Location = New System.Drawing.Point(14, 83)
        Me.dgvTruthTable.Name = "dgvTruthTable"
        Me.dgvTruthTable.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvTruthTable.RowHeadersVisible = False
        Me.dgvTruthTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.dgvTruthTable.RowTemplate.Height = 21
        Me.dgvTruthTable.Size = New System.Drawing.Size(292, 177)
        Me.dgvTruthTable.TabIndex = 28
        '
        'dgvTimer
        '
        Me.dgvTimer.AllowUserToAddRows = False
        Me.dgvTimer.AllowUserToDeleteRows = False
        Me.dgvTimer.AllowUserToResizeColumns = False
        Me.dgvTimer.AllowUserToResizeRows = False
        Me.dgvTimer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvTimer.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvTimer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colNo, Me.colTimer})
        Me.dgvTimer.Enabled = False
        Me.dgvTimer.Location = New System.Drawing.Point(312, 83)
        Me.dgvTimer.Name = "dgvTimer"
        Me.dgvTimer.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvTimer.RowHeadersVisible = False
        Me.dgvTimer.RowTemplate.Height = 21
        Me.dgvTimer.Size = New System.Drawing.Size(139, 177)
        Me.dgvTimer.TabIndex = 30
        '
        'chkTimer
        '
        Me.chkTimer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkTimer.AutoSize = True
        Me.chkTimer.Location = New System.Drawing.Point(312, 63)
        Me.chkTimer.Name = "chkTimer"
        Me.chkTimer.Size = New System.Drawing.Size(79, 16)
        Me.chkTimer.TabIndex = 29
        Me.chkTimer.Text = "With Timer"
        Me.chkTimer.UseVisualStyleBackColor = True
        '
        'colNo
        '
        Me.colNo.DataPropertyName = "NO"
        Me.colNo.HeaderText = "#"
        Me.colNo.Name = "colNo"
        Me.colNo.ReadOnly = True
        Me.colNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colNo.Width = 30
        '
        'colTimer
        '
        Me.colTimer.DataPropertyName = "DELAY"
        Me.colTimer.HeaderText = "Delay"
        Me.colTimer.Name = "colTimer"
        Me.colTimer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colTimer.Width = 80
        '
        'ctrlSysInput
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.dgvTimer)
        Me.Controls.Add(Me.chkTimer)
        Me.Controls.Add(Me.lblTruthTable)
        Me.Controls.Add(Me.dgvTruthTable)
        Me.Controls.Add(Me.lblnput)
        Me.Controls.Add(Me.txtInput)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.lblOutput)
        Me.Name = "ctrlSysInput"
        Me.Size = New System.Drawing.Size(463, 275)
        CType(Me.dgvTruthTable, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTimer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblnput As System.Windows.Forms.Label
    Friend WithEvents txtInput As System.Windows.Forms.TextBox
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox
    Friend WithEvents lblOutput As System.Windows.Forms.Label
    Friend WithEvents lblTruthTable As System.Windows.Forms.Label
    Friend WithEvents dgvTruthTable As System.Windows.Forms.DataGridView
    Friend WithEvents dgvTimer As System.Windows.Forms.DataGridView
    Friend WithEvents chkTimer As System.Windows.Forms.CheckBox
    Friend WithEvents colNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTimer As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
