﻿Module mdlMain

#Region "Declarations"

    Public dtSystems As DataTable

    Public dcLstInput As Dictionary(Of String, ArrayList)

    Public projName As String = ""

#End Region

    Public Sub initDtSystems()
        'create new dtSystems
        dtSystems = New DataTable
        dtSystems.Columns.Add("NAME")
        dtSystems.Columns.Add("INPUT")
        dtSystems.Columns.Add("OUTPUT")

        dtSystems.Columns("NAME").DefaultValue = ""
        dtSystems.Columns("INPUT").DefaultValue = "0"
        dtSystems.Columns("OUTPUT").DefaultValue = "0"
    End Sub

    Public Sub refreshDtSystems()
        dtSystems.Clear()
        dtSystems.Rows.Add(dtSystems.NewRow)
    End Sub

End Module
