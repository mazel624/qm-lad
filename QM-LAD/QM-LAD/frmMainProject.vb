﻿Public Class frmMainProject

#Region "Declarations"

    Dim frmCtr As Integer = 0
    Private Const COL_SYSNAME_IDX As Integer = 0
    Private Const COL_INPUT_IDX As Integer = COL_SYSNAME_IDX + 1
    Private Const COL_OUTPUT_IDX As Integer = COL_INPUT_IDX + 1
    Private Const COL_BTNVIEW_IDX As Integer = COL_OUTPUT_IDX + 1

#End Region

#Region "Initialization"

    Private Sub init()
        initDtSystems()

        dgvInput.DataSource = dtSystems
        mdlUtil.refreshDGV(dgvInput)

    End Sub

#End Region

#Region "Events"

#Region "Menu Buttons"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ' clear all tables and variables
        mdlMain.refreshDtSystems()
        mdlUtil.refreshDGV(dgvInput)

        txtProject.Text = "Project 1"
        txtTotalInput.Text = "0"
        txtTotalOutput.Text = "0"
    End Sub

    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen.Click

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

    End Sub

    Private Sub btnSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSettings.Click

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub btnSetInput_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetInput.Click
        mdlMain.projName = txtProject.Text
        frmSysInput.ShowDialog()
    End Sub

#End Region

#Region "DGV related buttons"

    Private Sub btnAddRow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddRow.Click
        dtSystems.Rows.Add(dtSystems.NewRow)

        mdlUtil.refreshDGV(dgvInput)
    End Sub

    Private Sub btnCompute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompute.Click
        Dim totalInput As Integer = 0
        Dim totalOutput As Integer = 0
        With dtSystems
            For i As Integer = 0 To .Rows.Count - 1
                Dim val As Integer = 0
                If .Rows(i).Item(COL_INPUT_IDX) IsNot DBNull.Value AndAlso Not String.IsNullOrEmpty(.Rows(i).Item(COL_INPUT_IDX).ToString) Then
                    val = Integer.Parse(.Rows(i).Item(COL_INPUT_IDX))
                End If
                totalInput = totalInput + val

                val = 0
                If .Rows(i).Item(COL_OUTPUT_IDX) IsNot DBNull.Value AndAlso Not String.IsNullOrEmpty(.Rows(i).Item(COL_OUTPUT_IDX).ToString) Then
                    val = Integer.Parse(.Rows(i).Item(COL_OUTPUT_IDX))
                End If
                totalOutput = totalOutput + val
            Next
        End With

        txtTotalInput.Text = totalInput
        txtTotalOutput.Text = totalOutput

    End Sub

    Private Sub btnDeleteRow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteRow.Click
        If dgvInput.SelectedCells.Count = 0 Then
            MessageBox.Show("Select row to be deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        If dgvInput.Rows.Count = 1 Then
            MessageBox.Show("Cannot delete selected row. Project needs at least ONE system.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        Dim slctRowIdx As Integer = dgvInput.SelectedCells(0).RowIndex
        dtSystems.Rows.RemoveAt(slctRowIdx)

        mdlUtil.refreshDGV(dgvInput)
    End Sub


#End Region

#Region "Form Events"

    Private Sub MainForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        init()


        
    End Sub

#End Region

#End Region


#Region "Methods"

    Private Sub startSimulation()
        'Dim key As String = dgvInput.Rows(e.RowIndex).Cells(COL_SYSNAME_IDX).Value

        'Dim frm As OutputDiagramForm
        'If Not mdlMain.mapScreens.ContainsKey(key) Then
        '    frm = New OutputDiagramForm
        '    frm.Text = "Project " + txtProject.Text
        '    frm.lblTitle.Text = "System " + key
        '    frm.scrnKey = key

        '    frm.txtInput.Text = dgvInput.Rows(e.RowIndex).Cells(COL_INPUT_IDX).Value
        '    frm.txtOutput.Text = dgvInput.Rows(e.RowIndex).Cells(COL_OUTPUT_IDX).Value

        '    frm.dgvTimer.Rows.Add(Integer.Parse(frm.txtOutput.Text))

        '    mdlMain.mapScreens.Add(key, frm)
        'Else
        '    frm = mdlMain.mapScreens.Item(key)
        'End If

        'frm.Visible = True
        'frm.Select()
    End Sub

#End Region


End Class