﻿Module mdlUtil

    Public arrBinInput As ArrayList

    Public Sub refreshDGV(ByRef dgv As DataGridView)
        dgv.ClearSelection()
        dgv.Refresh()
    End Sub

    'Public Function Bin_To_Dec(ByVal Bin As String) 'function to convert a binary number to decimal
    '    Dim dec As Double = Nothing
    '    Dim length As Integer = Len(Bin)
    '    Dim temp As Integer = Nothing
    '    Dim x As Integer = Nothing
    '    For x = 1 To length
    '        temp = Val(Mid(Bin, length, 1))
    '        length = length - 1
    '        If temp <> "0" Then
    '            dec += (2 ^ (x - 1))
    '        End If
    '    Next

    '    Return dec
    'End Function

    'Public Sub convertInput(ByVal input As String, ByVal inputCnt As Integer)
    '    Dim arrInput() As String
    '    arrInput = Split(input, ",")

    '    Dim arrBin(arrInput.Length) As String
    '    Dim bitSize As Integer = 0
    '    For intCount As Long = LBound(arrInput) To UBound(arrInput)
    '        Dim binary As String = Convert.ToString(Long.Parse(Trim(arrInput(intCount))), 2)
    '        binary = binary.PadLeft(inputCnt, "0"c)

    '        Debug.Print(Trim(arrInput(intCount)) & "  " & binary)
    '        arrBin(intCount) = binary
    '        If bitSize < binary.Length Then
    '            bitSize = binary.Length
    '        End If
    '        'arrBinInput.Add(Convert.ToString(Long.Parse(Trim(arrInput(intCount))), 2))
    '    Next

    'End Sub

    Public Function convertString2Int(ByVal value As String) As Integer
        Try
            Return Integer.Parse(value)
        Catch ex As Exception
            Return 0
        End Try
    End Function

End Module
