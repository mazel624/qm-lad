﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMainProject
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.Label1 = New System.Windows.Forms.Label
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSettings = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnOpen = New System.Windows.Forms.Button
        Me.btnNew = New System.Windows.Forms.Button
        Me.panInput = New System.Windows.Forms.Panel
        Me.btnSetInput = New System.Windows.Forms.Button
        Me.lblTotalOutputs = New System.Windows.Forms.Label
        Me.txtTotalOutput = New System.Windows.Forms.TextBox
        Me.lblTotalInput = New System.Windows.Forms.Label
        Me.btnDeleteRow = New System.Windows.Forms.Button
        Me.btnCompute = New System.Windows.Forms.Button
        Me.btnAddRow = New System.Windows.Forms.Button
        Me.dgvInput = New System.Windows.Forms.DataGridView
        Me.colSystemName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colInputCnt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colOutputCnt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtTotalInput = New System.Windows.Forms.TextBox
        Me.txtProject = New System.Windows.Forms.TextBox
        Me.lblSystem = New System.Windows.Forms.Label
        Me.lblProject = New System.Windows.Forms.Label
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.panInput.SuspendLayout()
        CType(Me.dgvInput, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer1.Size = New System.Drawing.Size(763, 450)
        Me.SplitContainer1.SplitterDistance = 37
        Me.SplitContainer1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Arial Black", 15.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(763, 37)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Project Information"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SplitContainer2
        '
        Me.SplitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.btnExit)
        Me.SplitContainer2.Panel1.Controls.Add(Me.btnSettings)
        Me.SplitContainer2.Panel1.Controls.Add(Me.btnSave)
        Me.SplitContainer2.Panel1.Controls.Add(Me.btnOpen)
        Me.SplitContainer2.Panel1.Controls.Add(Me.btnNew)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.panInput)
        Me.SplitContainer2.Size = New System.Drawing.Size(763, 409)
        Me.SplitContainer2.SplitterDistance = 94
        Me.SplitContainer2.TabIndex = 0
        '
        'btnExit
        '
        Me.btnExit.Image = Global.QM_LAD.My.Resources.Resources._Exit
        Me.btnExit.Location = New System.Drawing.Point(6, 326)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(80, 74)
        Me.btnExit.TabIndex = 1
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSettings
        '
        Me.btnSettings.Image = Global.QM_LAD.My.Resources.Resources.Settings
        Me.btnSettings.Location = New System.Drawing.Point(6, 246)
        Me.btnSettings.Name = "btnSettings"
        Me.btnSettings.Size = New System.Drawing.Size(80, 74)
        Me.btnSettings.TabIndex = 0
        Me.btnSettings.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Image = Global.QM_LAD.My.Resources.Resources.Save
        Me.btnSave.Location = New System.Drawing.Point(6, 166)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 74)
        Me.btnSave.TabIndex = 0
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnOpen
        '
        Me.btnOpen.Image = Global.QM_LAD.My.Resources.Resources.Open
        Me.btnOpen.Location = New System.Drawing.Point(6, 86)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(80, 74)
        Me.btnOpen.TabIndex = 0
        Me.btnOpen.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Image = Global.QM_LAD.My.Resources.Resources._New
        Me.btnNew.Location = New System.Drawing.Point(6, 6)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(80, 74)
        Me.btnNew.TabIndex = 0
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'panInput
        '
        Me.panInput.Controls.Add(Me.btnSetInput)
        Me.panInput.Controls.Add(Me.lblTotalOutputs)
        Me.panInput.Controls.Add(Me.txtTotalOutput)
        Me.panInput.Controls.Add(Me.lblTotalInput)
        Me.panInput.Controls.Add(Me.btnDeleteRow)
        Me.panInput.Controls.Add(Me.btnCompute)
        Me.panInput.Controls.Add(Me.btnAddRow)
        Me.panInput.Controls.Add(Me.dgvInput)
        Me.panInput.Controls.Add(Me.txtTotalInput)
        Me.panInput.Controls.Add(Me.txtProject)
        Me.panInput.Controls.Add(Me.lblSystem)
        Me.panInput.Controls.Add(Me.lblProject)
        Me.panInput.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panInput.Location = New System.Drawing.Point(0, 0)
        Me.panInput.Name = "panInput"
        Me.panInput.Size = New System.Drawing.Size(663, 407)
        Me.panInput.TabIndex = 0
        '
        'btnSetInput
        '
        Me.btnSetInput.Image = Global.QM_LAD.My.Resources.Resources.Edit2
        Me.btnSetInput.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSetInput.Location = New System.Drawing.Point(525, 341)
        Me.btnSetInput.Name = "btnSetInput"
        Me.btnSetInput.Size = New System.Drawing.Size(127, 53)
        Me.btnSetInput.TabIndex = 11
        Me.btnSetInput.Text = "Input Values"
        Me.btnSetInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSetInput.UseVisualStyleBackColor = True
        '
        'lblTotalOutputs
        '
        Me.lblTotalOutputs.AutoSize = True
        Me.lblTotalOutputs.Location = New System.Drawing.Point(6, 314)
        Me.lblTotalOutputs.Name = "lblTotalOutputs"
        Me.lblTotalOutputs.Size = New System.Drawing.Size(69, 12)
        Me.lblTotalOutputs.TabIndex = 10
        Me.lblTotalOutputs.Text = "Total Output"
        '
        'txtTotalOutput
        '
        Me.txtTotalOutput.Location = New System.Drawing.Point(81, 311)
        Me.txtTotalOutput.Name = "txtTotalOutput"
        Me.txtTotalOutput.ReadOnly = True
        Me.txtTotalOutput.Size = New System.Drawing.Size(70, 19)
        Me.txtTotalOutput.TabIndex = 9
        '
        'lblTotalInput
        '
        Me.lblTotalInput.AutoSize = True
        Me.lblTotalInput.Location = New System.Drawing.Point(15, 291)
        Me.lblTotalInput.Name = "lblTotalInput"
        Me.lblTotalInput.Size = New System.Drawing.Size(60, 12)
        Me.lblTotalInput.TabIndex = 8
        Me.lblTotalInput.Text = "Total Input"
        '
        'btnDeleteRow
        '
        Me.btnDeleteRow.Image = Global.QM_LAD.My.Resources.Resources.DelRow
        Me.btnDeleteRow.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteRow.Location = New System.Drawing.Point(560, 284)
        Me.btnDeleteRow.Name = "btnDeleteRow"
        Me.btnDeleteRow.Size = New System.Drawing.Size(90, 40)
        Me.btnDeleteRow.TabIndex = 7
        Me.btnDeleteRow.Text = "Del Row"
        Me.btnDeleteRow.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteRow.UseVisualStyleBackColor = True
        '
        'btnCompute
        '
        Me.btnCompute.Image = Global.QM_LAD.My.Resources.Resources.Sum
        Me.btnCompute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCompute.Location = New System.Drawing.Point(368, 284)
        Me.btnCompute.Name = "btnCompute"
        Me.btnCompute.Size = New System.Drawing.Size(90, 40)
        Me.btnCompute.TabIndex = 7
        Me.btnCompute.Text = "Total I/O"
        Me.btnCompute.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCompute.UseVisualStyleBackColor = True
        '
        'btnAddRow
        '
        Me.btnAddRow.Image = Global.QM_LAD.My.Resources.Resources.AddRow
        Me.btnAddRow.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAddRow.Location = New System.Drawing.Point(464, 284)
        Me.btnAddRow.Name = "btnAddRow"
        Me.btnAddRow.Size = New System.Drawing.Size(90, 40)
        Me.btnAddRow.TabIndex = 7
        Me.btnAddRow.Text = "Add Row"
        Me.btnAddRow.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAddRow.UseVisualStyleBackColor = True
        '
        'dgvInput
        '
        Me.dgvInput.AllowUserToAddRows = False
        Me.dgvInput.AllowUserToDeleteRows = False
        Me.dgvInput.AllowUserToResizeColumns = False
        Me.dgvInput.AllowUserToResizeRows = False
        Me.dgvInput.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvInput.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colSystemName, Me.colInputCnt, Me.colOutputCnt})
        Me.dgvInput.Location = New System.Drawing.Point(16, 69)
        Me.dgvInput.Name = "dgvInput"
        Me.dgvInput.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvInput.RowHeadersVisible = False
        Me.dgvInput.RowTemplate.Height = 21
        Me.dgvInput.Size = New System.Drawing.Size(634, 209)
        Me.dgvInput.TabIndex = 6
        '
        'colSystemName
        '
        Me.colSystemName.DataPropertyName = "NAME"
        DataGridViewCellStyle13.NullValue = "System 1"
        Me.colSystemName.DefaultCellStyle = DataGridViewCellStyle13
        Me.colSystemName.HeaderText = "System Name"
        Me.colSystemName.MinimumWidth = 200
        Me.colSystemName.Name = "colSystemName"
        Me.colSystemName.Width = 350
        '
        'colInputCnt
        '
        Me.colInputCnt.DataPropertyName = "INPUT"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "N0"
        DataGridViewCellStyle14.NullValue = "0"
        Me.colInputCnt.DefaultCellStyle = DataGridViewCellStyle14
        Me.colInputCnt.HeaderText = "No. of Inputs"
        Me.colInputCnt.MinimumWidth = 80
        Me.colInputCnt.Name = "colInputCnt"
        Me.colInputCnt.Width = 80
        '
        'colOutputCnt
        '
        Me.colOutputCnt.DataPropertyName = "OUTPUT"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle15.Format = "N0"
        DataGridViewCellStyle15.NullValue = "0"
        Me.colOutputCnt.DefaultCellStyle = DataGridViewCellStyle15
        Me.colOutputCnt.HeaderText = "No. of Output"
        Me.colOutputCnt.MinimumWidth = 80
        Me.colOutputCnt.Name = "colOutputCnt"
        Me.colOutputCnt.Width = 80
        '
        'txtTotalInput
        '
        Me.txtTotalInput.Location = New System.Drawing.Point(81, 288)
        Me.txtTotalInput.Name = "txtTotalInput"
        Me.txtTotalInput.ReadOnly = True
        Me.txtTotalInput.Size = New System.Drawing.Size(70, 19)
        Me.txtTotalInput.TabIndex = 1
        '
        'txtProject
        '
        Me.txtProject.Location = New System.Drawing.Point(17, 28)
        Me.txtProject.Name = "txtProject"
        Me.txtProject.Size = New System.Drawing.Size(633, 19)
        Me.txtProject.TabIndex = 1
        Me.txtProject.Text = "Project 1"
        '
        'lblSystem
        '
        Me.lblSystem.AutoSize = True
        Me.lblSystem.Location = New System.Drawing.Point(14, 54)
        Me.lblSystem.Name = "lblSystem"
        Me.lblSystem.Size = New System.Drawing.Size(49, 12)
        Me.lblSystem.TabIndex = 0
        Me.lblSystem.Text = "Systems"
        '
        'lblProject
        '
        Me.lblProject.AutoSize = True
        Me.lblProject.Location = New System.Drawing.Point(15, 13)
        Me.lblProject.Name = "lblProject"
        Me.lblProject.Size = New System.Drawing.Size(74, 12)
        Me.lblProject.TabIndex = 0
        Me.lblProject.Text = "Project Name"
        '
        'frmMainProject
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(763, 450)
        Me.Controls.Add(Me.SplitContainer1)
        Me.MaximizeBox = False
        Me.Name = "frmMainProject"
        Me.Text = "MQM-LAD System Version 1.0"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.panInput.ResumeLayout(False)
        Me.panInput.PerformLayout()
        CType(Me.dgvInput, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSettings As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnOpen As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents panInput As System.Windows.Forms.Panel
    Friend WithEvents dgvInput As System.Windows.Forms.DataGridView
    Friend WithEvents lblSystem As System.Windows.Forms.Label
    Friend WithEvents txtProject As System.Windows.Forms.TextBox
    Friend WithEvents lblProject As System.Windows.Forms.Label
    Friend WithEvents btnDeleteRow As System.Windows.Forms.Button
    Friend WithEvents btnAddRow As System.Windows.Forms.Button
    Friend WithEvents lblTotalOutputs As System.Windows.Forms.Label
    Friend WithEvents txtTotalOutput As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalInput As System.Windows.Forms.Label
    Friend WithEvents txtTotalInput As System.Windows.Forms.TextBox
    Friend WithEvents btnCompute As System.Windows.Forms.Button
    Friend WithEvents btnSetInput As System.Windows.Forms.Button
    Friend WithEvents colSystemName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colInputCnt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colOutputCnt As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
