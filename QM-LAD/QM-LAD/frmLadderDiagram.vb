﻿Public Class frmLadderDiagram

    Private dcOutput As Dictionary(Of String, ArrayList)
    Private dcTimer As Dictionary(Of String, ArrayList)
    Private dcMaxOutCnt As Dictionary(Of String, Integer)

    Private maxOutCnt As Integer
    Private lstOutput As ArrayList
    Private lstTimer As ArrayList ' >> arraylist of time.. if value is 0, no timer needed... if * at the end, dependency EXISTS

    Public WriteOnly Property DcOutputList() As Dictionary(Of String, ArrayList)
        Set(ByVal value As Dictionary(Of String, ArrayList))
            dcOutput = value
        End Set
    End Property

    Public WriteOnly Property DcTimerList() As Dictionary(Of String, ArrayList)
        Set(ByVal value As Dictionary(Of String, ArrayList))
            dcTimer = value
        End Set
    End Property

    Public WriteOnly Property DcMaxOutputCount() As Dictionary(Of String, Integer)
        Set(ByVal value As Dictionary(Of String, Integer))
            dcMaxOutCnt = value
        End Set
    End Property

    
    Private Sub frmLadderDiagram_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        initializeTabOutput()

    End Sub

    Private Sub initializeTabOutput()

        For i As Integer = 0 To dtSystems.Rows.Count - 1
            ' create user control
            Dim dgvLadder As New DgvLadderDiagram
            dgvLadder.Dock = DockStyle.Fill
            dgvLadder.MaxOutputCount = dcMaxOutCnt.Item("sys" & i)
            dgvLadder.ListOutput = dcOutput.Item("sys" & i)
            dgvLadder.ListTimer = dcTimer.Item("sys" & i)

            If dgvLadder.ListTimer.Count > 0 Then
                dgvLadder.MaxOutputCount = dgvLadder.MaxOutputCount + 1
            End If

            Dim pg As TabPage
            If i >= TabControl1.TabPages.Count Then
                ' create new tab
                pg = New TabPage
                pg.Name = "sys" & i
                pg.Text = dtSystems.Rows(i).Item("NAME").ToString
                TabControl1.TabPages.Add(pg)
            Else
                pg = TabControl1.TabPages(i)
                pg.Name = "sys" & i
                pg.Text = dtSystems.Rows(i).Item("NAME").ToString
            End If

            'add page to tab
            TabControl1.TabPages("sys" & i).Controls.Add(dgvLadder)
            dgvLadder.start()

        Next
    End Sub

End Class
